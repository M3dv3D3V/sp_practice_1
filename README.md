## 1 Собираем образ из Dockerfile
### make build
## 2 Поднимаем контейнер в фоновом режиме
### make up
## 3 Запускаем терминал в работающем контейнере
### docker exec -it be71412a9e92 sh
## 4 Запускаем скрипт
### sh file_checker.sh

### docker rm $(docker ps -aq) - удалить контейнеры
### docker rmi -f $(docker images -aq) - удалить images
