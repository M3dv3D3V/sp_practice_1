start:
	docker-compose up -d

stop:
	docker-compose down

reload:
	docker-compose down
	docker-compose up

build:
	docker-compose build

rebuild:
	docker-compose build
	docker-compose up
