#!/bin/bash

echo "[INFO] Author - Zahar Medvedev"
echo "[INFO] Program name - File checker"
echo "[INFO] Description - this program checks for the existence of a file."

directory_name=$(pwd)
echo "[INFO] Current directory name - $directory_name"


while :
do
    if [[ "$filename" = "" ]]
        echo "[INFO] Enter filename:"
        then read filename
    fi
    # checking the existence of the file
    if [[ -f "$filename" || -d "$filename" ]]; then
        echo "[INFO] $filename exists."
    else 
        echo "[ERROR] $filename does not exist."
        echo "[INFO] Continue checking the file? (y/N)"
        read is_new_file
        if [[ "$is_new_file" = "N" ]]; then
            echo "[ERROR] returned an error code -1"
            break
        fi
        continue
    fi

    if [[ "$username" = "" ]]
        echo "[INFO] Enter username: "
        then read username
    fi
    # checking the existence of the user
    is_exist_user=$(awk -F: '{print $1}' /etc/passwd | grep "$username")
    if [[ "$is_exist_user" = "$username" ]]; then
        echo "[INFO] $username exist."
    else
        echo "[ERROR] $username does not exist."
        echo "[INFO] Continue checking the file? (y/N)"
        read is_new_file
        if [[ "$is_new_file" = "N" ]]; then
            echo "[ERROR] returned an error code -1"
            break
        fi
        continue
    fi

    file_permission_owner=$(ls -l $filename | awk '{print substr($1, 2, 3)}')
    file_owner=$(ls -l $filename | awk '{print $3}')

    file_group=$(ls -l $filename | awk '{print $4}')
    file_permission_group=$(ls -l $filename | awk '{print substr($1, 5, 3)}')

    file_permission_other=$(ls -l $filename | awk '{print substr($1, 8, 3)}')

    case $username in
        $file_owner)
            echo "[INFO] Permissions $username for $filename: $file_permission_owner"
            ;;
        $file_group)
            echo "[INFO] Permissions $username for $filename: $file_permission_group"
            ;;
        *)
            echo "[INFO] Permissions $username for $filename: $file_permission_other"
            ;;
        esac

    echo "[INFO] Continue checking the file? (y/N)"
    read is_new_file
    if [[ "$is_new_file" = "N" ]]; then
        break
    fi

done